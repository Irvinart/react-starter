const gulp = require('gulp');
const nodemon = require('gulp-nodemon');

gulp.task('default', () => {
    nodemon({
        script: 'app.js',
        ext: 'js',
        env: {
            PORT: 7777,
        },
        ingone: ['./node_modules/**'],
    })
    .on('restart', () => {
        console.log('Restarted!');
    });
});
