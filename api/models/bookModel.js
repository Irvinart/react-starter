const mongoose = require('mongoose');

const bookModel = new mongoose.Schema({
    title: {
        type: String,
    },
    autor: {
        type: String,
    },
    genre: {
        type: String,
    },
    read: {
        type: Boolean,
        default: false,
    },
});

module.exports = mongoose.model('book', bookModel);
