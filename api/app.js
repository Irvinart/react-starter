const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const bookRouter = require('./routes/bookRoutes');
const Book = require('./models/bookModel');

const db = mongoose.connect('mongodb://localhost/bookAPI');
const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api/books', bookRouter(Book));

app.get('/', (req, res) => {
    res.send('Welcome to my API!!!');
});

app.listen(port, () => {
    console.log(`App is running on port ${port}`);
});
