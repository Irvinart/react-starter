const express = require('express');

function routes(Book) {
    const bookRouter = express.Router();

    bookRouter.route('/')
        .get((req, res) => {
            Book.find((err, books) => {
                if (err) {
                    console.warn('err', err);
                } else {
                    res.json(books);
                }
            });
        })
        .post((req, res) => {
            const book = new Book(req.body);
    
            book.save();
            res.status(201).send(book);
        });
    
    bookRouter.route('/:bookId')
        .get((req, res) => {
            Book.findById(req.params.bookId, (err, book) => {
                if (err) {
                    console.warn('err', err);
                } else {
                    res.json(book);
                }
            });
        });

    return bookRouter;
}

module.exports = routes;
