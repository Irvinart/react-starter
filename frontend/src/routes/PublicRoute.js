import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import type { ComponentType } from 'react';

import { queryString } from '../helpers/url';
import paths from '../constants/paths';

export type routerProps = {
    location: {
        pathname: string,
        search: string,
    },
};

export type Props = {
    isLoggedIn: boolean,
} & routerProps;

export type cProps = {
    component: ComponentType<Props>,
    props: Props,
};

const PublicRoute = ({
    component: WrappedComponent,
    props: childProps,
    ...rest
}: cProps) => {
    const redirect = queryString('redirect');
    let path;
    if (redirect) path = redirect;
    else path = paths.index;

    const routeRender = props =>
        !childProps.isLoggedIn ? (
            <WrappedComponent {...props} {...childProps} />
        ) : (
            <Redirect to={path} />
        );

    return <Route {...rest} render={routeRender} />;
};

export default PublicRoute;
