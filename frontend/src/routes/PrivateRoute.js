// @flow
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import type { ComponentType } from 'react';

import paths from '../constants/paths';

export type routerProps = {
    location: {
        pathname: string,
        search: string,
    },
};

export type Props = {
    isLoggedIn: boolean,
} & routerProps;

export type cProps = {
    component: ComponentType<Props>,
    props: Props,
};

const PrivateRoute = ({
    component: WrappedComponent,
    props: childProps,
    ...rest
}: cProps) => {
    const routeRender = (props: routerProps) => {
        return childProps.isLoggedIn ? (
            <WrappedComponent {...props} {...childProps} />
        ) : (
            <Redirect
                to={`${paths.auth.signin}?redirect=${props.location.pathname}${
                    props.location.search
                }`}
            />
        );
    };

    return <Route {...rest} render={routeRender} />;
};

export default PrivateRoute;
