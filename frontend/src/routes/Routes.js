// @flow
import React from 'react';
import { Switch } from 'react-router-dom';

import paths from '../constants/paths';
// import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import Login from '../containers/auth/LoginContainer/LoginContainer';
import Employees from '../containers/employee/EmployeeListContainer/EmployeeListContainer';
import EmployeeView from '../components/employee/SingleEmployee/SingleEmployee';
import EmployeeChange from '../containers/employee/CreateEmployeeContainer/CreateEmployeeContainer';
import AttendanceView from '../containers/employee/AttendanceContainer/AttendanceContainer';

export type Props = {
    childProps: Object,
};

export default ({ childProps }: Props) => {
    return (
        <Switch>
            <PublicRoute
                props={childProps}
                component={Login}
                exact
                path={paths.index}
            />
            <PublicRoute
                props={childProps}
                component={Login}
                exact
                path={paths.auth.signin}
            />
            <PublicRoute
                props={childProps}
                component={Employees}
                exact
                path={paths.employees.index}
            />
            <PublicRoute
                props={childProps}
                component={EmployeeChange}
                exact
                path={paths.employees.add}
            />
            <PublicRoute
                props={childProps}
                component={EmployeeChange}
                exact
                path={paths.employees.edit}
            />
            <PublicRoute
                props={childProps}
                component={EmployeeView}
                exact
                path={paths.employees.view}
            />
            <PublicRoute
                props={childProps}
                component={AttendanceView}
                exact
                path={paths.attendance.index}
            />
        </Switch>
    );
};
