// @flow

import React from 'react';
import { withRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { connect } from 'react-redux';
import LinearProgress from 'material-ui/LinearProgress';
import * as authAction from './actions/auth';
import Header from './components/common/Header/Header';
import Footer from './components/common/Footer/Footer';
import Routes from './routes/Routes';

import globalStyles from './styles/global.module.scss';

type Props = {
    userLoggedIn: boolean,
    fetching: boolean,
    userLogout: () => void,
};

const divStyle = {
    backgroundColor: '#fff',
};

const App = ({ userLoggedIn, fetching, userLogout }: Props) => (
    <div className={globalStyles.page}>
        <Header logout={userLogout} userLoggedIn={userLoggedIn} />
        {fetching && <LinearProgress mode="indeterminate" style={divStyle} />}
        <ToastContainer />
        <main className={globalStyles.content}>
            <Routes childProps={{ isLoggedIn: userLoggedIn }} />
        </main>
        <Footer />
    </div>
);

const mapStateToProps = state => ({
    fetching: state.user.fetching,
    userLoggedIn: state.user.loggedIn,
});

const mapDispatchToProps = dispatch => ({
    userLogout: () => dispatch(authAction.userLogoutRequest()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
