import * as actions from '../constants/redux/employeeActions';
import { filterDataTableSuccess } from '../actions/employee';

export const employeesMiddleware = store => next => action => {
    const { employee: { employeeList } } = store.getState();

    switch (action.type) {
        case actions.FILTER_EMPLOYEE_LIST_REQUEST: {
            let filteredList = employeeList.slice();
            const criteria = action.payload;
            filteredList = filteredList.filter(item => {
                return (
                    (criteria.name
                        ? item.name.includes(criteria.name)
                        : true) &&
                    (criteria.age
                        ? parseInt(item.age, 10) === parseInt(criteria.age, 10)
                        : true) &&
                    (criteria.position
                        ? item.position === criteria.position
                        : true)
                );
            });
            return store.dispatch(filterDataTableSuccess(filteredList));
        }

        default:
            return next(action);
    }
};
