// @flow
// EMPLOYEE - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';
import Request from '../services/Request';
import API from '../constants/endpoints';

// REDUX
import * as employeeActions from '../actions/employee';
import * as employeeConstants from '../constants/redux/employeeActions';

import isEmpty from '../helpers/isObjectEmpty';

export function* getEmployeeList(): Generator<PutEffect, *, *> {
    try {
        const payload = yield call(Request, { route: API.employees.getList });
        yield put(employeeActions.getEmployeeListSuccess(payload));
    } catch (error) {
        const message = error instanceof Error ? error.toString() : error;
        yield put(employeeActions.getEmployeeListFailure(message));
    }
}

export function* getFilters(): Generator<PutEffect, *, *> {
    try {
        const payload = yield call(Request, {
            route: API.settings.filtersList,
        });
        yield put(employeeActions.getEmployeeFiltersSuccess(payload));
    } catch (error) {
        const message = error instanceof Error ? error.toString() : error;
        yield put(employeeActions.getEmployeeFiltersFailure(message));
    }
}

export function* addEmployee(action: Object): Generator<PutEffect, *, *> {
    const { payload: { values, resolve, reject } } = action;
    try {
        const newEmployee = Object.assign({}, values);
        const { employeeList, filterCriteria } = yield select(
            state => state.employee
        );
        const newList = employeeList.slice();
        newList.push(newEmployee);

        yield put(employeeActions.addEmployeeSuccess(newList));

        if (!isEmpty(filterCriteria))
            yield put(employeeActions.filterDataTableRequest(filterCriteria));
        yield call(resolve);
    } catch (e) {
        console.log(e);
        yield call(reject, new SubmissionError({ _error: e }));
        yield put(employeeActions.addEmployeeFailure(e));
    }
}

export function* editEmployee(action: Object): Generator<PutEffect, *, *> {
    const { payload: { values, resolve, reject } } = action;
    try {
        const editedEmployee = Object.assign({}, values);
        const { employeeList, filterCriteria } = yield select(
            state => state.employee
        );

        const editedList = employeeList.map(item => {
            if (item.id === editedEmployee.id) {
                return Object.assign(item, editedEmployee);
            }
            return item;
        });

        yield put(employeeActions.editEmployeeSuccess(editedList));

        if (!isEmpty(filterCriteria))
            yield put(employeeActions.filterDataTableRequest(filterCriteria));
        yield call(resolve);
    } catch (e) {
        console.log(e);
        yield call(reject, new SubmissionError({ _error: e }));
        yield put(employeeActions.editEmployeeFailure(e));
    }
}

// WATCHERS
export function* watchEmployeeSaga(): Generator<IOEffect, *, *> {
    yield takeLatest(
        employeeConstants.GET_EMPLOYEE_LIST_REQUEST,
        getEmployeeList
    );
    yield takeLatest(
        employeeConstants.GET_EMPLOYEE_FILTERS_REQUEST,
        getFilters
    );
    yield takeLatest(employeeConstants.ADD_EMPLOYEE_REQUEST, addEmployee);
    yield takeLatest(employeeConstants.EDIT_EMPLOYEE_REQUEST, editEmployee);
}
