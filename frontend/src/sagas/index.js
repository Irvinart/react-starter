// @flow
// INDEX - SAGAS
// =============================================================================
// Creates a single Saga to be consumed by the Redux store.

import { all } from 'redux-saga/effects';

import { supervise } from '../helpers/saga';
import * as user from './user';
import * as employee from './employee';

const defaultStrategy = {
    logErrors: process.env.DEV_TOOLS,
    logRestarts: process.env.DEV_TOOLS,
};

// ROOT SAGA
export default function* rootSaga(): Generator<*, *, *> {
    yield all([supervise({ ...defaultStrategy }, user.watchPostLogin)]);
    yield all([supervise({ ...defaultStrategy }, employee.watchEmployeeSaga)]);
}
