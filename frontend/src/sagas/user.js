// @flow
// USER - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeLatest } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';

import Request from '../services/Request';
import API from '../constants/endpoints';
// import type { Credentials } from '../types/auth';

// REDUX
import * as authActions from '../actions/auth';
import * as authConstants from '../constants/redux/authActions';

export function* postLogin(action: Object): Generator<PutEffect, *, *> {
    try {
        // const credentials: Credentials = action.payload.values;
        const payload = yield call(Request, {
            route: API.login,
            method: 'GET',
            // TODO: change here when the backend will be ready
            // method: 'POST',
            // data: credentials,
        });
        console.log(payload);
        yield put(authActions.postLoginSuccess(payload));
        action.payload.resolve();
    } catch (error) {
        const message = error instanceof Error ? error.toString() : error;
        action.payload.reject(new SubmissionError(message));
        yield put(authActions.postLoginFailure(message));
    }
}

// WATCHERS
export function* watchPostLogin(): Generator<IOEffect, *, *> {
    yield takeLatest(authConstants.POST_LOGIN_REQUEST, postLogin);
}
