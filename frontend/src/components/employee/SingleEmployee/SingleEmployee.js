import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom';

import * as employeeActions from '../../../actions/employee';
import paths from '../../../constants/paths';

import type { SingleEmployeeProps } from '../../../types/employee';

import styles from './SingleEmployee.module.scss';

export class SingleEmployee extends Component<SingleEmployeeProps> {
    componentDidMount() {
        const { employeeList, getEmployees } = this.props;
        if (!employeeList.length) getEmployees();
    }

    renderPlaceholder = () => <div>Loading...</div>;

    render() {
        const { employeeId, employeeList } = this.props;

        const employee = employeeList.find(one => one.id === employeeId);

        return employee ? (
            <div className={styles.container}>
                <h1 className={styles.title}>
                    {`View ${employee.name}'s profile`}
                </h1>
                <Card className={styles.card}>
                    <CardHeader
                        title={employee.name}
                        subtitle={`${employee.age} y.o. ${employee.position}`}
                        avatar={employee.avatar}
                        actAsExpander
                        showExpandableButton
                    />
                    <CardActions>
                        <FlatButton
                            label="Edit"
                            containerElement={
                                <Link
                                    to={paths.variable.employees.edit(
                                        employee.id
                                    )}
                                />
                            }
                        />
                        <FlatButton
                            label="Delete"
                            containerElement={<span />}
                        />
                    </CardActions>
                    <CardText expandable>{employee.description}</CardText>
                </Card>
                <RaisedButton
                    containerElement={<Link to={paths.employees.index} />}
                    label="Back to list"
                />
            </div>
        ) : (
            this.renderPlaceholder()
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    employeeId: ownProps.match.params.employeeId,
    employeeList: state.employee.employeeList,
});

const mapDispatchToProps = dispatch => ({
    getEmployees: () => dispatch(employeeActions.getEmployeeListRequest()),
});

export const ConnectedSingleEmployee = connect(
    mapStateToProps,
    mapDispatchToProps
)(SingleEmployee);

export default ConnectedSingleEmployee;
