// @flow
import * as React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

import paths from '../../../constants/paths';
import { actions, actionsMap } from '../../../constants/common';

import type { EmployeeActionsProps } from '../../../types/employee';

import styles from './EmployeeActions.module.scss';

const buttonStyle = {
    minWidth: 'inherit',
};

const labelStyle = {
    fontSize: '12px',
    paddingLeft: '5px',
    paddingRight: '5px',
};

const handleClickFunction = (event: Event, callback: any => any): any => {
    event.stopPropagation();
    callback();
};

function EmployeeActions({
    accessLevel,
    employee,
    history,
}: EmployeeActionsProps) {
    return (
        <React.Fragment>
            {actionsMap[accessLevel].includes(actions.VIEW) && (
                <RaisedButton
                    style={buttonStyle}
                    labelStyle={labelStyle}
                    className={styles.button}
                    label={actions.VIEW}
                    onClick={e =>
                        handleClickFunction(e, () =>
                            history.push(
                                paths.variable.employees.view(employee.id)
                            )
                        )
                    }
                />
            )}
            {actionsMap[accessLevel].includes(actions.EDIT) && (
                <RaisedButton
                    style={buttonStyle}
                    labelStyle={labelStyle}
                    className={styles.button}
                    primary
                    label={actions.EDIT}
                    onClick={e =>
                        handleClickFunction(e, () =>
                            history.push(
                                paths.variable.employees.edit(employee.id)
                            )
                        )
                    }
                />
            )}
            {actionsMap[accessLevel].includes(actions.DELETE) && (
                <RaisedButton
                    style={buttonStyle}
                    labelStyle={labelStyle}
                    className={styles.button}
                    secondary
                    label={actions.DELETE}
                    onClick={e =>
                        handleClickFunction(e, () =>
                            console.log(
                                `Deleting ${employee.name} with ID: ${
                                    employee.id
                                }`
                            )
                        )
                    }
                />
            )}
            {actionsMap[accessLevel].includes(actions.COMMENT) && (
                <RaisedButton
                    style={buttonStyle}
                    labelStyle={labelStyle}
                    className={styles.button}
                    label={actions.COMMENT}
                    onClick={e =>
                        handleClickFunction(e, () =>
                            console.log(
                                `Commenting ${employee.name} with ID: ${
                                    employee.id
                                }`
                            )
                        )
                    }
                />
            )}
            {actionsMap[accessLevel].includes(actions.KICK) && (
                <RaisedButton
                    style={buttonStyle}
                    labelStyle={labelStyle}
                    className={styles.button}
                    label={actions.KICK}
                    onClick={e =>
                        handleClickFunction(e, () =>
                            console.log(`Kikcking ${employee.name}'s`)
                        )
                    }
                />
            )}
        </React.Fragment>
    );
}

export default EmployeeActions;
