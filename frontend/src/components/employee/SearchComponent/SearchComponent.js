import React from 'react';
import AutoComplete from 'material-ui/AutoComplete';

import type { SearchComponentProps } from '../../../types/employee';

const SearchComponent = ({
    source,
    onSearch,
    hint,
    floatingLabel,
    maxResults,
}: SearchComponentProps) => (
    <div>
        <AutoComplete
            filter={AutoComplete.caseInsensitiveFilter}
            dataSource={source}
            hintText={hint}
            floatingLabelText={floatingLabel}
            onUpdateInput={onSearch}
            fullWidth
            maxSearchResults={maxResults}
        />
    </div>
);

SearchComponent.defaultProps = {
    maxResults: 5,
};

export default SearchComponent;
