import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import { accessLevels } from '../../../constants/common';
import paths from '../../../constants/paths';
import EmployeeActions from '../EmployeeActions/EmployeeActions';

import styles from './List.module.scss';

import type { ListProps } from '../../../types/employee';

const List = ({ user, employeeList, history }: ListProps) => (
    <div className={styles.container}>
        <Table
            onCellClick={row =>
                history.push(
                    paths.variable.employees.view(employeeList[row].id)
                )
            }
        >
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                <TableRow>
                    <TableHeaderColumn>Name</TableHeaderColumn>
                    <TableHeaderColumn>Age</TableHeaderColumn>
                    <TableHeaderColumn>Position</TableHeaderColumn>
                    <TableHeaderColumn>Actions</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                {employeeList.map(employee => (
                    <TableRow key={employee.id}>
                        <TableRowColumn>{employee.name}</TableRowColumn>
                        <TableRowColumn>{employee.age}</TableRowColumn>
                        <TableRowColumn>{employee.position}</TableRowColumn>
                        <TableRowColumn>
                            <EmployeeActions
                                accessLevel={
                                    user.accessLevel || accessLevels.DEV
                                }
                                employee={employee}
                                history={history}
                            />
                        </TableRowColumn>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </div>
);

export default List;
