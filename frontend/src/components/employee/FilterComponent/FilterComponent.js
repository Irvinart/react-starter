// @flow
import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'material-ui';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import type {
    FilterComponentProps,
    FilterInstance,
    FilterEmployeeParams,
} from '../../../types/employee';

import styles from './FilterComponent.module.scss';

type State = {
    fields: Object,
};

class FilterComponent extends Component<FilterComponentProps, State> {
    constructor(props: FilterComponentProps) {
        super(props);

        this.state = {
            fields: {},
        };
    }

    componentWillMount() {
        const fields = {};
        // eslint-disable-next-line
        for (const key of this.props.filters) {
            fields[key.slug] = null || '';
        }

        this.setState({ fields });
    }

    handleReset = () => {
        const emptyState = Object.assign({}, this.state.fields);
        // eslint-disable-next-line
        for (const key in emptyState) {
            // eslint-disable-next-line
            if (emptyState.hasOwnProperty(key)) {
                emptyState[key] = '';
            }
        }
        this.setState({ fields: emptyState });
        this.props.applyFilters(emptyState);
    };

    renderField = (field: FilterInstance) => {
        const floatingLabel = field.label;
        const hint = `Filter by ${field.label}`;

        if (field.values && Array.isArray(field.values)) {
            return (
                <SelectField
                    key={field.slug}
                    floatingLabelText={floatingLabel}
                    hintText={hint}
                    value={this.state.fields[field.slug]}
                    onChange={(event, index, value) =>
                        this.handleFilterChange({ [field.slug]: value })
                    }
                    name={field.slug}
                >
                    <MenuItem value={null} primaryText="" />
                    {field.values.map(value => (
                        <MenuItem
                            key={value}
                            value={value}
                            primaryText={value}
                        />
                    ))}
                </SelectField>
            );
        }
        return (
            <TextField
                key={field.slug}
                hintText={hint}
                floatingLabelText={floatingLabel}
                type="text"
                value={this.state.fields[field.slug]}
                name={field.slug}
                onChange={(event, value) =>
                    this.handleFilterChange({ [field.slug]: value })
                }
            />
        );
    };

    handleFilterChange = (fieldValue: FilterEmployeeParams): void => {
        this.setState({
            fields: {
                ...this.state.fields,
                ...fieldValue,
            },
        });
    };

    render() {
        const { filters, applyFilters } = this.props;

        return (
            <div className={styles.container}>
                <div className={styles.fieldsContainer}>
                    {filters.map(field => this.renderField(field))}
                </div>
                <div className={styles.controlsContainer}>
                    <RaisedButton
                        label="Apply Filters"
                        className={styles.button}
                        onClick={() => applyFilters(this.state.fields)}
                    />
                    <RaisedButton
                        label="Reset Filters"
                        secondary
                        className={styles.button}
                        onClick={() => this.handleReset(this.state.fields)}
                    />
                </div>
            </div>
        );
    }
}

export default FilterComponent;
