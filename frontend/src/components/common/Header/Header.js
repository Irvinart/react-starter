// @flow
import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import HeaderMenu from '../HeaderMenu/HeaderMenu';

import type { HeaderProps } from '../../../types/common';

import styles from './Header.module.scss';

const Header = ({ userLoggedIn, logout }: HeaderProps) => (
    <header className={styles.container}>
        <div className={styles.innerWrapper}>
            {userLoggedIn && (
                <div className="header__logout" key="logout">
                    <FlatButton
                        label="Logout"
                        onClick={() => logout()}
                        style={{ color: 'white' }}
                    />
                </div>
            )}
            <HeaderMenu />
        </div>
    </header>
);

export default Header;
