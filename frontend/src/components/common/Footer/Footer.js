import React from 'react';
import moment from 'moment';

import styles from './Footer.module.scss';

const Footer = () => {
    return (
        <footer className={styles.footer}>
            Morebis &copy; {moment(Date.now()).format('Y')}
        </footer>
    );
};

export default Footer;
