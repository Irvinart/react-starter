// @flow
import React from 'react';
import { Link } from 'react-router-dom';

import paths from '../../../constants/paths';

import styles from './HeaderMenu.module.scss';

const HeaderMenu = () => {
    return (
        <div className={styles.container}>
            <Link className={styles.link} to={paths.attendance.index}>
                Attendance
            </Link>
            <Link className={styles.link} to={paths.employees.index}>
                Employee List
            </Link>
        </div>
    );
};

export default HeaderMenu;
