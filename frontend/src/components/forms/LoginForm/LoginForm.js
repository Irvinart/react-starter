// @flow
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { TextField, RaisedButton } from 'material-ui';
import { reduxForm, Field } from 'redux-form';
import cn from 'classnames';

import type { Element } from 'react';
import type { LoginFormProps, TextFieldProps } from '../../../types/forms';

// Actions
import * as authActions from '../../../actions/auth';
import validateEmail from '../../../helpers/validateEmail';

import styles from './LoginForm.module.scss';

class LoginForm extends Component<LoginFormProps> {
    styles = {
        underlineStyle: {
            borderColor: '#0099DA',
        },
        floatingLabelFocusStyle: {
            color: '#0099DA',
        },
    };

    renderTextField = ({
        input,
        floatingLabel,
        label,
        meta: { touched, error },
        ...rest
    }: TextFieldProps): Element<TextField> => (
        <TextField
            hintText={label}
            floatingLabelText={floatingLabel}
            errorText={touched && error}
            {...input}
            {...rest}
        />
    );

    handleOnSubmitForm = (values: Object) =>
        new Promise((resolve, reject) =>
            this.props.login({ values, resolve, reject })
        );

    render() {
        const { handleSubmit, pristine, submitting, className } = this.props;
        return (
            <form
                className={cn(styles.container, className)}
                onSubmit={handleSubmit(this.handleOnSubmitForm)}
            >
                <Field
                    name="username"
                    component={this.renderTextField}
                    label="Enter your username"
                    floatingLabel="Username"
                    autoFocus
                    className={styles.input}
                    underlineFocusStyle={this.styles.underlineStyle}
                    floatingLabelFocusStyle={
                        this.styles.floatingLabelFocusStyle
                    }
                />
                <Field
                    name="password"
                    component={this.renderTextField}
                    label="Enter your password"
                    floatingLabel="Password"
                    type="password"
                    className={styles.input}
                    underlineFocusStyle={this.styles.underlineStyle}
                    floatingLabelFocusStyle={
                        this.styles.floatingLabelFocusStyle
                    }
                />
                <RaisedButton
                    className={styles.submitButton}
                    label="Submit"
                    primary
                    type="submit"
                    disabled={pristine || submitting}
                />
            </form>
        );
    }
}

const mapStateToProps = state => ({
    fetching: state.user.fetching,
});

const mapDispatchToProps = dispatch => ({
    login: credentials => dispatch(authActions.postLoginRequest(credentials)),
});

export const ReduxLoginForm = reduxForm({
    form: 'loginForm',
    validate: values => {
        const errors = {};

        if (!values.username) errors.username = 'This field is required';
        else if (!validateEmail(values.username))
            errors.username = 'Email is incorrect';

        if (!values.password) errors.password = 'This field is required';

        return errors;
    },
})(LoginForm);

export const ConnectedReduxLoginForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(ReduxLoginForm);

export default withRouter(ConnectedReduxLoginForm);
