import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';

import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';

import type { Element } from 'react';
import type { TextFieldProps, EmployeeFormProps } from '../../../types/forms';

import isEmpty from '../../../helpers/isObjectEmpty';

import styles from './EmployeeForm.module.scss';

type State = {
    fields: Array<any>,
};

export class EmployeeForm extends Component<EmployeeFormProps, State> {
    constructor(props: EmployeeFormProps) {
        super(props);

        this.state = {
            fields: [],
        };
    }

    styles = {
        underlineStyle: {
            borderColor: '#0099DA',
        },
        floatingLabelFocusStyle: {
            color: '#0099DA',
        },
    };

    componentWillReceiveProps(nextProps: EmployeeFormProps) {
        if (nextProps.options.length) {
            this.setState({ fields: nextProps.options });
        }
    }

    renderTextField = ({
        input,
        floatingLabel,
        label,
        meta: { touched, error },
        ...rest
    }: TextFieldProps): Element<TextField> => (
        <TextField
            hintText={label}
            floatingLabelText={floatingLabel}
            errorText={touched && error}
            {...input}
            {...rest}
        />
    );

    renderSelectField = ({
        input,
        floatingLabel,
        label,
        meta: { touched, error },
        ...rest
    }: TextFieldProps): Element<SelectField> => {
        const optionsObject = this.props.options.find(
            o => o.slug === input.name
        );
        return (
            <SelectField
                {...input}
                hintText={label}
                floatingLabelText={floatingLabel}
                errorText={touched && error}
                onChange={(event, index, value) => input.onChange(value)}
                {...rest}
            >
                {!isEmpty(optionsObject) &&
                    optionsObject.values.map(value => (
                        <MenuItem
                            key={value}
                            value={value}
                            primaryText={value}
                        />
                    ))}
            </SelectField>
        );
    };

    render() {
        const { handleSubmit, formSubmit } = this.props;
        return (
            <form
                className={styles.container}
                onSubmit={handleSubmit(formSubmit)}
            >
                <Field
                    name="name"
                    component={this.renderTextField}
                    label="Enter Employee name"
                    floatingLabel="Name"
                    autoFocus
                    className={styles.field}
                    underlineFocusStyle={this.styles.underlineStyle}
                    floatingLabelFocusStyle={
                        this.styles.floatingLabelFocusStyle
                    }
                />
                {!!this.state.fields.length && (
                    <Field
                        name="age"
                        component={this.renderSelectField}
                        label="Enter employee age"
                        floatingLabel="Age"
                        className={styles.field}
                        underlineFocusStyle={this.styles.underlineStyle}
                        floatingLabelFocusStyle={
                            this.styles.floatingLabelFocusStyle
                        }
                        maxHeight={250}
                    />
                )}
                {!!this.state.fields.length && (
                    <Field
                        name="position"
                        component={this.renderSelectField}
                        label="Select employee position"
                        floatingLabel="Position"
                        className={styles.field}
                        underlineFocusStyle={this.styles.underlineStyle}
                        floatingLabelFocusStyle={
                            this.styles.floatingLabelFocusStyle
                        }
                        maxHeight={250}
                    />
                )}
                <RaisedButton
                    label={isEmpty(this.props.initialValues) ? 'Add' : 'Edit'}
                    className={styles.buttonSubmit}
                    type="submit"
                />
            </form>
        );
    }
}

export default reduxForm({
    form: 'createEmployeeForm',
    validate: values => {
        const errors = {};

        if (!values.name) errors.name = 'Required field';
        if (!values.age) errors.age = 'Required field';
        if (!values.position) errors.position = 'Required field';

        return errors;
    },
})(EmployeeForm);
