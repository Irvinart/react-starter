// @flow
import { createSelector } from 'reselect';

export const getUserLoggedIn = (state: Object) =>
    state.user ? state.user.loggedIn : undefined;

export const getApiPath = (state: Object) =>
    state.environment ? state.environment.apiPath : undefined;

export const getAccessToken = (state: Object) =>
    state.user ? state.user.accessToken : undefined;

export const getApiParams = createSelector(
    getApiPath,
    getAccessToken,
    (apiPath, accessToken) => ({ apiPath, accessToken }),
);
