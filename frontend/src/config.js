export default {
    development: {
        API_DOMAIN: '/mocks',
    },

    production: {
        API_DOMAIN: 'http://morebis.ck.ua:8186/api/v1',
    },
};
