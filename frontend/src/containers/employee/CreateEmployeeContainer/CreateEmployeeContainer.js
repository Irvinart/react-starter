import React, { Component } from 'react';
import { connect } from 'react-redux';

import Form from '../../../components/forms/EmployeeForm/EmployeeForm';
import * as employeeActions from '../../../actions/employee';
import type { CreateEmployeeContainerProps } from '../../../types/employee';

import generateId from '../../../helpers/generateId';
import paths from '../../../constants/paths';

import styles from './CreateEmployeeContainer.scss';

type State = {
    employeeId: string,
};

export class CreateEmployeeContainer extends Component<
    CreateEmployeeContainerProps,
    State
> {
    constructor(props: CreateEmployeeContainerProps) {
        super(props);

        this.state = {
            employeeId: '',
        };
    }

    componentWillMount() {
        const {
            match: { params: { employeeId } },
            employeeList,
            getEmployees,
            selectFields,
            getFilters,
        } = this.props;
        if (!selectFields.length) getFilters();
        if (employeeId) {
            this.setState({ employeeId });
            if (!employeeList.length) getEmployees();
        }
    }

    handleCreateFormSubmit = values => {
        if (!values.id) Object.assign(values, { id: generateId() });

        return new Promise((resolve, reject) => {
            this.props.addEmployee({ values, resolve, reject });
        }).then(() => this.props.history.push(paths.employees.index));
    };

    handleEditFormSubmit = values => {
        return new Promise((resolve, reject) => {
            this.props.editEmployee({ values, resolve, reject });
        }).then(() => this.props.history.push(paths.employees.index));
    };

    render() {
        const { employeeList, selectFields } = this.props;
        const { employeeId } = this.state;

        const employee = employeeId
            ? employeeList.find(e => e.id === employeeId)
            : null;

        const title = employee
            ? `Edit ${employee.name}`
            : 'Create new Employee';

        return (
            <div className={styles.container}>
                <h1 className={styles.title}>{title}</h1>
                {selectFields && (
                    <Form
                        form={
                            employeeId
                                ? `Form-${employeeId}`
                                : 'CreateEmployeeForm'
                        }
                        initialValues={employee}
                        options={selectFields}
                        formSubmit={
                            employeeId
                                ? this.handleEditFormSubmit
                                : this.handleCreateFormSubmit
                        }
                    />
                )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    employeeList: state.employee.employeeList,
    selectFields: state.employee.filters,
});

const mapDispatchToProps = dispatch => ({
    getFilters: () => dispatch(employeeActions.getEmployeeFiltersRequest()),
    getEmployees: () => dispatch(employeeActions.getEmployeeListRequest()),
    addEmployee: formData =>
        dispatch(employeeActions.addEmployeeRequest(formData)),
    editEmployee: formData =>
        dispatch(employeeActions.editEmployeeRequest(formData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(
    CreateEmployeeContainer
);
