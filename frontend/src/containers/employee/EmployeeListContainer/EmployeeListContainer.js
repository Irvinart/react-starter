// @flow
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import FilterListIcon from 'material-ui/svg-icons/content/filter-list';
import AddIcon from 'material-ui/svg-icons/content/add';

import type {
    EmployeeListContainerProps,
    FilterEmployeeParams,
} from '../../../types/employee';
import List from '../../../components/employee/List/List';
import FilterComponent from '../../../components/employee/FilterComponent/FilterComponent';
import * as employeeActions from '../../../actions/employee';
import paths from '../../../constants/paths';

// import Search from '../../components/employee/SearchComponent'; // TODO: is Search component really needed?

import styles from './EmployeeListContainer.module.scss';

type State = {
    isFilterExpanded: boolean,
};

export class EmployeesComponent extends Component<
    EmployeeListContainerProps,
    State
> {
    constructor(props: EmployeeListContainerProps) {
        super(props);

        this.state = {
            isFilterExpanded: false,
        };
    }

    componentDidMount() {
        const { employees, getEmployees, filtersList, getFilters } = this.props;
        if (!employees.length) getEmployees();
        if (!filtersList.length) getFilters();
    }

    /* TODO: is Search component really needed? */
    // handleSearch = (value: string) => {
    //     console.log(value);
    // };

    handleToggleFilters = () => {
        this.setState(prevState => ({
            isFilterExpanded: !prevState.isFilterExpanded,
        }));
    };

    render() {
        const { history, user, filtersList, applyFilters } = this.props;
        const { isFilterExpanded } = this.state;

        return (
            <div className={styles.container}>
                <h1 className={styles.title}>Employee List</h1>
                {/* TODO: is Search component really needed? */}
                {/* <Search source={employees.map(one => one.name)} onSearch={this.handleSearch} hint="Search employee by name" floatingLabel="Employee name" maxResults={10} /> */}
                <div className={styles.actions}>
                    <FloatingActionButton
                        className={styles.button}
                        containerElement={
                            <Link
                                className={styles.button}
                                to={paths.employees.add}
                            />
                        }
                        mini
                    >
                        <AddIcon />
                    </FloatingActionButton>
                    {filtersList && (
                        <FloatingActionButton
                            className={styles.button}
                            mini
                            secondary
                            onClick={this.handleToggleFilters}
                        >
                            <FilterListIcon />
                        </FloatingActionButton>
                    )}
                </div>
                {isFilterExpanded && (
                    <FilterComponent
                        filters={filtersList}
                        applyFilters={applyFilters}
                    />
                )}
                {this.props.employees && (
                    <List
                        employeeList={this.props.employees}
                        user={user}
                        history={history}
                    />
                )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    employees: state.employee.filteredList,
    filtersList: state.employee.filters,
});

const mapDispatchToProps = dispatch => ({
    getEmployees: () => dispatch(employeeActions.getEmployeeListRequest()),
    getFilters: () => dispatch(employeeActions.getEmployeeFiltersRequest()),
    applyFilters: (query?: FilterEmployeeParams) =>
        dispatch(employeeActions.filterDataTableRequest(query)),
});

export const ConnectedEmployeesComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(EmployeesComponent);

export default withRouter(ConnectedEmployeesComponent);
