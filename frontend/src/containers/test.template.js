// @flow
import React from 'react';

const TestTemplate = () => (
    <div>
        <h1>Delete this template after adding new component</h1>
    </div>
);

export default TestTemplate;
