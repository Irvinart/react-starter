// @flow
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import LoginForm from '../../../components/forms/LoginForm/LoginForm';

import type { LoginContainerProps } from '../../../types/auth';

import styles from './LoginContainer.module.scss';

export class LoginContainer extends Component<LoginContainerProps> {
    redirectTo = (path: string) => this.props.history.push(path);

    componentDidMount() {
        if (!this.props.userLoggedIn) return;
        if (this.props.userLoggedIn) {
            this.redirectTo('/accounts');
        }
    }

    componentWillReceiveProps(nextProps: LoginContainerProps) {
        if (nextProps.errorAuth && !this.props.errorAuth) {
            toast.error(nextProps.errorAuth, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true,
                closeButton: false,
            });
        }

        if (nextProps.userLoggedIn) {
            this.redirectTo('/accounts');
        }
    }

    render() {
        return (
            <div>
                <div className={styles.container}>
                    <div className={styles.titleContainer}>
                        <h2 className={styles.title}>Please log in</h2>
                    </div>
                    <div className={styles.innerWrapper}>
                        <LoginForm />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    errorAuth: state.user.error,
    userLoggedIn: state.user.loggedIn,
});

// const mapDispatchToProps = dispatch => ({
//
// });

// todo: add 'mapDispatchToProps' to connect
export default withRouter(connect(mapStateToProps)(LoginContainer));
