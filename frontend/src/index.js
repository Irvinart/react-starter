import 'isomorphic-fetch';
import React from 'react';
// noinspection JSUnresolvedVariable
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { ConnectedRouter } from 'react-router-redux';

import App from './app';
import { history, configureStore } from './store';
import registerServiceWorker from './registerServiceWorker';

require('./styles/main.css');

// Material UI theme config
const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#0099DA',
        primary2Color: '#0099DA',
        primary3Color: '#0099DA',
        accent1Color: '#da0000',
        accent2Color: '#da0000',
        accent3Color: '#da0000',
        pickerHeaderColor: '#0099DA',
    },
});

const performRender = async () => {
    const store = await configureStore();
    render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <App />
                </MuiThemeProvider>
            </ConnectedRouter>
        </Provider>,
        document.getElementById('root')
    );
};

performRender();
registerServiceWorker();
