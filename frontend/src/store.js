import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import promise from 'redux-promise-middleware';
import { persistStore, autoRehydrate } from 'redux-persist';
import { createFilter } from 'redux-persist-transform-filter';
import createHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';
import rootSaga from './sagas';

import reducer from './reducers';
import { employeesMiddleware } from './middlewares/employee';

const localStorageWhitelist = ['user'];

const saveUserFilter = createFilter('user', ['accessToken', 'loggedIn']);
const loadUserFilter = createFilter('user', null, ['accessToken', 'loggedIn']);

export const history = createHistory();

const persistStorePromisified = (configuredStore, options) =>
    new Promise(resolve =>
        persistStore(configuredStore, options, () => resolve())
    );

export const configureStore = async () => {
    const reduxRouterMiddleware = routerMiddleware(history);
    const sagaMiddleware = createSagaMiddleware();
    const middleware = composeWithDevTools(
        autoRehydrate({}),
        applyMiddleware(
            promise(),
            sagaMiddleware,
            employeesMiddleware,
            reduxRouterMiddleware
        )
    );
    const preloadedState = {};
    const configuredStore = createStore(reducer, preloadedState, middleware);

    // Configure redux-saga
    configuredStore.runSaga = sagaMiddleware.run;
    configuredStore.close = () => configuredStore.dispatch(END);
    configuredStore.runSaga(rootSaga);

    // Configure redux-persist
    await persistStorePromisified(configuredStore, {
        whitelist: localStorageWhitelist,
        transforms: [saveUserFilter, loadUserFilter],
    });

    return configuredStore;
};
