// @flow
import config from '../config';
import type { RequestHeaders, RequestProps } from '../types/common';

const getHeaders = (): Headers => {
    const token = window.localStorage.getItem('token');

    const headers: RequestHeaders = {
        Accept: 'application/json, text/json',
        'Content-Type': 'application/json',
    };

    if (token) headers.Authorization = `Bearer ${token}`;

    return headers;
};

const domain = config[process.env.NODE_ENV].API_DOMAIN;

const handleExceptions = (response: Response) => {
    console.log(response);
    if (response.status >= 400) {
        return response.text().then(text => {
            let error = text;
            try {
                error = JSON.parse(text);
                error.response = response;
            } catch (e) {
                console.info(
                    'Error could not be parsed as JSON, falling back to text'
                );
            }
            throw error;
        });
    }
    return response;
};

const handleSuccessResponse = (response: Response): any => {
    if (!response) {
        return Promise.reject(new Error('No response supplied to handleBody'));
    }

    const contentType: string = response.headers.get('Content-Type');
    switch (contentType.toLowerCase()) {
        case 'application/json':
        case 'application/json; charset=utf-8':
        case 'text/json':
            try {
                return response.json();
            } catch (error) {
                return Promise.reject(
                    new Error(`Got a JSON response, but it couldn't be parsed`)
                );
            }

        case 'text/plain':
        case 'text/html':
            // These may be returned on a 500 status, or other unexpected outcome.
            try {
                return response.text().then(text => ({ text }));
            } catch (error) {
                return Promise.reject(
                    new Error(`Got a text response, but it couldn't be parsed`)
                );
            }

        default:
            return Promise.reject(
                new Error(`Unrecognized Content-Type: ${contentType}`)
            );
    }
};

const Request = ({
    route,
    method = 'GET',
    data,
    headers,
}: RequestProps): any => {
    const defaultHeaders = getHeaders();

    let body;

    if (data) {
        if (typeof data === 'string') {
            body = data;
        } else if (typeof data === 'object') {
            body = JSON.stringify(data);
        }
    }

    const url = `${domain}${route}`;

    return fetch(url, {
        method,
        mode: 'cors',
        headers: {
            ...defaultHeaders,
            ...headers,
        },
        body,
    })
        .then(response => {
            let handler;
            if (response.status >= 400) {
                handler = handleExceptions(response);
            } else {
                handler = handleSuccessResponse(response);
            }
            return handler;
        })
        .then(result => result);
};

export default Request;
