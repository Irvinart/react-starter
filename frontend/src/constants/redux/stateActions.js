// @flow
// STATE - ACTION TYPES
// =============================================================================

export const REHYDRATE_FROM_LOCAL_STORAGE_FINISHED =
    'persist/REHYDRATE_FROM_LOCAL_STORAGE_FINISHED';
