// AUTH - ACTION TYPES
// =============================================================================

export const POST_LOGIN_REQUEST = '[AUTH] POST_LOGIN_REQUEST';
export const POST_LOGIN_SUCCESS = '[AUTH] POST_LOGIN_SUCCESS';
export const POST_LOGIN_FAILURE = '[AUTH] POST_LOGIN_FAILURE';
export const USER_LOGOUT_REQUEST = '[AUTH] USER_LOGOUT_REQUEST';
