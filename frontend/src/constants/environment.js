// @flow
// CONSTANTS - ENVIRONMENT
// =============================================================================

export type Environment = {
    apiPath: string
};

export const development: Environment = {
    apiPath: 'http://morebis.ck.ua:8186',
};

export const production: Environment = {
    apiPath: '',
};
