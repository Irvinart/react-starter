const endpoints = {
    development: {
        login: '/users/token.json',
        signup: '/signup',
        employees: {
            getList: '/employees/list.json',
            getOne: id => `/employees/${id}`,
            add: '/employees/add',
            editOne: id => `/employees/${id}/edit`,
            deleteOne: id => `/employees/${id}/delete`,
        },
        settings: {
            filtersList: '/settings/filters.json',
        },
    },
    production: {
        login: '/users/token/',
        signup: '/signup',
        employees: {
            getList: '/employees',
            getOne: id => `/employees/${id}`,
            add: '/employees/add',
            editOne: id => `/employees/${id}/edit`,
            deleteOne: id => `/employees/${id}/delete`,
        },
        settings: {
            filtersList: '/filters',
        },
    },
};

export default endpoints[process.env.NODE_ENV];
