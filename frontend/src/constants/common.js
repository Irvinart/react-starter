export const actions = {
    VIEW: 'View',
    EDIT: 'Edit',
    DELETE: 'Delete',
    COMMENT: 'Comment',
    KICK: 'Kick',
};

export const accessLevels = {
    READ: 'READ',
    WRITE: 'WRITE',
    ADMIN: 'ADMIN',
    DEV: 'DEV',
};

export const actionsMap = {
    [accessLevels.READ]: [actions.VIEW, actions.COMMENT],
    [accessLevels.WRITE]: [actions.EDIT, actions.COMMENT],
    [accessLevels.ADMIN]: [actions.EDIT, actions.COMMENT, actions.DELETE],
    [accessLevels.DEV]: [
        actions.EDIT,
        actions.COMMENT,
        actions.DELETE,
        actions.KICK,
    ],
};
