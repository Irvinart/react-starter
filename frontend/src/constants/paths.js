const paths = {
    index: '/',
    auth: {
        signin: '/login',
        signup: '/signup',
    },
    variable: {
        employees: {
            edit: id => `/employees/edit/${id}`,
            view: id => `/employees/view/${id}`,
        },
    },
    employees: {
        index: '/employees',
        add: '/employees/add',
        edit: '/employees/edit/:employeeId',
        view: '/employees/view/:employeeId',
    },
    attendance: {
        index: '/attendance',
    },
};

export default paths;
