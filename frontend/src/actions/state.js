import { createAction } from 'redux-actions';
import { REHYDRATE } from 'redux-persist/constants';

import { REHYDRATE_FROM_LOCAL_STORAGE_FINISHED } from '../constants/redux/stateActions';

// redux-persist dispatches these actions automatically.
export const rehydrate = createAction(REHYDRATE);
export const rehydrateFromLocalStorageFinished = createAction(
    REHYDRATE_FROM_LOCAL_STORAGE_FINISHED
);
