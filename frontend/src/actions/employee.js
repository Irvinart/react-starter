// EMP - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

// ACTION TYPES
import * as employeeConstants from '../constants/redux/employeeActions';

export const getEmployeeListRequest = createAction(
    employeeConstants.GET_EMPLOYEE_LIST_REQUEST
);
export const getEmployeeListSuccess = createAction(
    employeeConstants.GET_EMPLOYEE_LIST_SUCCESS
);
export const getEmployeeListFailure = createAction(
    employeeConstants.GET_EMPLOYEE_LIST_FAILURE
);

export const getEmployeeFiltersRequest = createAction(
    employeeConstants.GET_EMPLOYEE_FILTERS_REQUEST
);
export const getEmployeeFiltersSuccess = createAction(
    employeeConstants.GET_EMPLOYEE_FILTERS_SUCCESS
);
export const getEmployeeFiltersFailure = createAction(
    employeeConstants.GET_EMPLOYEE_FILTERS_FAILURE
);

export const addEmployeeRequest = createAction(
    employeeConstants.ADD_EMPLOYEE_REQUEST
);
export const addEmployeeSuccess = createAction(
    employeeConstants.ADD_EMPLOYEE_SUCCESS
);
export const addEmployeeFailure = createAction(
    employeeConstants.ADD_EMPLOYEE_FAILURE
);

export const editEmployeeRequest = createAction(
    employeeConstants.EDIT_EMPLOYEE_REQUEST
);
export const editEmployeeSuccess = createAction(
    employeeConstants.EDIT_EMPLOYEE_SUCCESS
);
export const editEmployeeFailure = createAction(
    employeeConstants.EDIT_EMPLOYEE_FAILURE
);

export const filterDataTableRequest = createAction(
    employeeConstants.FILTER_EMPLOYEE_LIST_REQUEST
);
export const filterDataTableSuccess = createAction(
    employeeConstants.FILTER_EMPLOYEE_LIST_SUCCESS
);

// export const searchEmployeeText = createAction(employeeTypes.SEARCH_EMPLOYEE_TEXT);
