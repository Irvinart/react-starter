// AUTH - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

// ACTION TYPES
import * as authConstants from '../constants/redux/authActions';

export const postLoginRequest = createAction(authConstants.POST_LOGIN_REQUEST);

export const postLoginSuccess = createAction(authConstants.POST_LOGIN_SUCCESS);

export const postLoginFailure = createAction(authConstants.POST_LOGIN_FAILURE);

export const userLogoutRequest = createAction(
    authConstants.USER_LOGOUT_REQUEST
);
