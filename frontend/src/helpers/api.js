// @flow
// Outputs param string pairs in the format 'foo=bar'
const createParamsPairString = (paramsPair) => {
    if (Array.isArray(paramsPair) && paramsPair.length === 2) {
        if (typeof paramsPair[0] !== 'string') {
            throw new TypeError('paramsPair[0] must be a string');
        }

        if (!['number', 'string', 'boolean'].includes(typeof paramsPair[1])) {
            throw new TypeError('paramsPair[1] must be a number, string, or boolean');
        }

        return `${encodeURIComponent(paramsPair[0])}=${encodeURIComponent(String(paramsPair[1]))}`;
    }

    throw new TypeError('paramsPair must be an array with two entries');
};

// Convert a 2D array into a query parameter string
const arrayToParamString = (paramsArray = []) => {
    if (Array.isArray(paramsArray)) {
        return paramsArray.map(createParamsPairString).join('&');
    }

    throw new TypeError('paramsArray must be an Array');
};

// Convert an object into a query parameter string
const objectToParamString = (paramsObject = {}) => {
    if (paramsObject !== null && typeof paramsObject === 'object') {
        const entries = Object.keys(paramsObject).map(key => [
            key,
            paramsObject[key],
        ]);

        return arrayToParamString(entries);
    }

    throw new TypeError('paramsObject must be an Object');
};

const convertToParamString = (content: any) => {
    if (Array.isArray(content)) {
        return arrayToParamString(content);
    } else if (content !== null && typeof content === 'object') {
        return objectToParamString(content);
    }

    throw new Error('convertToParamString requires an array or an object as its first parameter');
};

export const convertQueryToString = (content: Object) => {
    const convertedString = convertToParamString(content);

    if (typeof convertedString === 'string') {
        if (convertedString.length) {
            return `?${convertedString}`;
        }

        // In the event that we successfully 'converted' an empty object or array -
        // which is completely legal - we should return an empty string.
        return '';
    }

    throw new Error('Query conversion failed');
};
