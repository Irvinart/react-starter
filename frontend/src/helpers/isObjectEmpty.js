/**
 * Returns true if specified object has no properties,
 * false otherwise.
 *
 * @param {object} object
 * @returns {boolean}
 */
const isObjectEmpty = object => {
    if (typeof object !== 'object') {
        return true;
        // throw new Error('Object must be specified.');
    }

    if (object === null) {
        return true;
    }

    if (Object.keys !== 'undefined') {
        // Using ECMAScript 5 feature.
        return Object.keys(object).length === 0;
    }

    // Using legacy compatibility mode.
    // for (const key in object) {
    //     if (object.hasOwnProperty(key)) {
    //         return false;
    //     }
    // }
    return true;
};

export default isObjectEmpty;
