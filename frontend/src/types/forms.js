import { FormProps, FieldProps } from 'redux-form/es';

export type TextFieldProps = FieldProps & {
    floatingLabel: string,
    label: string,
};

export type EmployeeFormProps = FormProps & {
    formSubmit: () => any,
};

export type LoginFormProps = FormProps & {
    className: string,
    login: ({
        values: {
            username: string,
            password: string,
        },
        resolve: () => any,
        reject: () => any,
    }) => any,
};
