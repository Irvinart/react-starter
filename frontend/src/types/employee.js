// Data Models
export type FilterEmployeeParams = {
    employeeName?: string,
    employeeAge?: string,
    employeePosition?: string,
};

export type FilterInstance = {
    slug: string,
    label: string,
    values?: Array<string> | string,
};

export type Employee = {
    id: string,
    name: string,
    age: string,
    position: string,
    avatar: string,
};

// Components Props
export type EmployeeActionsProps = {
    accessLevel: string,
    employee: Employee,
    history: Object,
};

export type ListProps = {
    user: Object,
    employeeList: Array<Employee>,
    history: Object,
};

export type SearchComponentProps = {
    source: string[],
    hint: string,
    floatingLabel: string,
    maxResults?: number,
    onSearch: () => any,
};

export type SingleEmployeeProps = {
    employeeId: string,
    history: Object,
    employeeList: Array<Employee>,
    getEmployees: () => any,
};

// Container Props
export type CreateEmployeeContainerProps = {
    employeeList: Array<Employee>,
    getEmployees: () => any,
    getFilters: () => any,
    addEmployee: () => any,
    editEmployee: () => any,
    selectFields: Array<FilterInstance>,
    history: Object,
    match: {
        params: Object,
    },
};

export type EmployeeListContainerProps = {
    employees: Array<Employee>,
    getEmployees: () => any,
    getFilters: () => Array<FilterInstance>,
    applyFilters: (query?: any) => any,
    history: Object,
    user: Object,
};

export type FilterComponentProps = {
    applyFilters: (query?: any) => any,
    filters: Array<FilterInstance>,
};
