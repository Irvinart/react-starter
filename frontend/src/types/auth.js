export type Credentials = {
    username: string,
    password: string,
};

export type LoginContainerProps = {
    className: string,
    history: Object,
    errorAuth: string,
    userLoggedIn: boolean,
    initialLoad: boolean,
    fetching: boolean,
};
