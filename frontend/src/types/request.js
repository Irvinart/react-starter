export type RequestHeaders = {
    Accept: string,
    'Content-Type':
        | 'application/json'
        | 'text/json'
        | 'application/x-www-form-urlencoded'
        | 'multipart/form-data',
    Authorization?: string,
};

export type RequestProps = {
    route: string,
    method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE',
    data?: Object | string,
    headers?: RequestHeaders,
};
