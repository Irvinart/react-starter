export type HeaderProps = {
    userLoggedIn: boolean,
    logout: () => void,
};
