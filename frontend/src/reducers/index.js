// @flow
import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import { routerReducer as router } from 'react-router-redux';

import user from './user';
import environment from './environment';
import employee from './employee';

export default combineReducers({
    environment,
    form,
    user,
    employee,
    router,
});
