// @flow
// ENVIRONMENT - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';
import { development, production } from '../constants/environment';

export const initialStateConfig = {
    apiPath:
        process.env.NODE_ENV === 'production'
            ? production.apiPath
            : development.apiPath,
};

export type EnvironmentState = {
    apiPath: string
};

export const initialState: EnvironmentState = {
    ...initialStateConfig,
};

const handlerMap = {};

export default handleActions(handlerMap, initialState);
