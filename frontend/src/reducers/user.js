// @flow
// USER - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as authConstants from '../constants/redux/authActions';

interface IUserState {
    accessToken: string;
    loggedIn: boolean;
    fetching: boolean;
    error: string;
}

export const initialState: IUserState = {
    accessToken: '',
    loggedIn: false,
    fetching: false,
    error: '',
};

const handlerMap = {
    [authConstants.POST_LOGIN_REQUEST]: state => ({
        ...state,
        accessToken: '',
        error: '',
        fetching: true,
    }),

    [authConstants.POST_LOGIN_SUCCESS]: (state, action) => ({
        accessToken: action.payload.token,
        error: '',
        fetching: false,
        loggedIn: true,
    }),

    [authConstants.POST_LOGIN_FAILURE]: (state, action) => ({
        ...state,
        accessToken: '',
        error: action.payload.non_field_errors.join('\n'),
        fetching: false,
    }),

    [authConstants.USER_LOGOUT_REQUEST]: () => ({
        ...initialState,
    }),
};

export default handleActions(handlerMap, initialState);
