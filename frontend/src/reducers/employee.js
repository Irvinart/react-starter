// @flow
// EMPLOYEE - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as employeeConstants from '../constants/redux/employeeActions';
import type { IEmployee, IFilterInstance } from '../types/employee';

interface IEmployeeState {
    employeeList: Array<IEmployee>;
    fetching: false;
    error: '';
    filteredList: Array<IEmployee>;
    filterCriteria: {};
    filters: Array<IFilterInstance>;
}
export const initialState: IEmployeeState = {
    employeeList: [],
    fetching: false,
    error: '',
    filteredList: [],
    filterCriteria: {},
    filters: [],
};

const handlerMap = {
    [employeeConstants.GET_EMPLOYEE_LIST_REQUEST]: state => ({
        ...state,
        error: '',
        fetching: true,
    }),

    [employeeConstants.GET_EMPLOYEE_LIST_SUCCESS]: (state, action) => ({
        ...state,
        employeeList: [...action.payload],
        filteredList: [...action.payload],
        error: '',
        fetching: false,
    }),

    [employeeConstants.GET_EMPLOYEE_LIST_FAILURE]: (state, action) => ({
        ...state,
        error: action.payload.non_field_errors.join('\n'),
        fetching: false,
    }),

    [employeeConstants.GET_EMPLOYEE_FILTERS_REQUEST]: state => ({
        ...state,
        filters: [],
    }),

    [employeeConstants.GET_EMPLOYEE_FILTERS_SUCCESS]: (state, action) => ({
        ...state,
        filters: action.payload,
    }),

    [employeeConstants.GET_EMPLOYEE_FILTERS_FAILURE]: (state, action) => ({
        ...state,
        filters: { error: action.payload },
    }),

    [employeeConstants.FILTER_EMPLOYEE_LIST_REQUEST]: (state, action) => ({
        ...state,
        filterCriteria: action.payload,
    }),

    [employeeConstants.FILTER_EMPLOYEE_LIST_SUCCESS]: (state, action) => ({
        ...state,
        filteredList: action.payload,
    }),

    [employeeConstants.ADD_EMPLOYEE_SUCCESS]: (state, action) => ({
        ...state,
        employeeList: [...action.payload],
        filteredList: [...action.payload],
    }),

    [employeeConstants.EDIT_EMPLOYEE_SUCCESS]: (state, action) => ({
        ...state,
        employeeList: [...action.payload],
        filteredList: [...action.payload],
    }),
};

export default handleActions(handlerMap, initialState);
