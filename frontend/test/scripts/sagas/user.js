// @flow
// AUTH (SAGA) - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */
import { expect } from 'chai';
import SagaTester from 'redux-saga-tester';
import fetchMock from 'fetch-mock';

import * as AuthSagas from '../../../src/sagas/user';
import * as AuthActions from '../../../src/actions/auth';
import * as AuthActionTypes from '../../../src/constants/redux/authTypes';

import authSuccessResponse from '../test_data/auth_success.json';
import authFailureResponse from '../test_data/auth_failure.json';

describe('SAGA: AUTH', () => {
    let sagaTester = null;

    const initialState = {
        environment: {
            apiPath: '/test',
        },
        user: {
            accessToken: '',
            loggedIn: false,
            fetching: false,
            error: '',
        },
    };

    const headers = { 'content-type': 'application/json' };
    const authCredentials = {
        username: 'example@mail.com',
        password: 'qwerty',
    };
    const user = (state = initialState.user) => state;
    const environment = (state = initialState.environment) => state;

    const reducers = { user, environment };

    beforeEach(() => {
        fetchMock.restore();
    });

    it(
        'should login - success',
        (async () => {
            const getResponseBody = authSuccessResponse;

            fetchMock.post('/test/api/v1/users/token/', {
                headers,
                body: getResponseBody,
            });

            sagaTester = new SagaTester({ initialState, reducers });
            sagaTester.start(AuthSagas.watchPostLogin);
            sagaTester.dispatch(AuthActions.postLoginRequest(authCredentials));

            await sagaTester.waitFor(AuthActionTypes.POST_LOGIN_SUCCESS);

            expect(sagaTester.getLatestCalledAction()).to.deep.equal({
                payload: getResponseBody,
                type: AuthActionTypes.POST_LOGIN_SUCCESS,
            });

            expect(
                sagaTester.wasCalled(AuthActionTypes.POST_LOGIN_SUCCESS)
            ).to.equal(true);
            expect(fetchMock.called()).to.be.true;
        }: any)
    );

    it(
        'should login - failure',
        (async () => {
            const getResponseBody = authFailureResponse;

            fetchMock.post('/test/api/v1/users/token/', {
                headers,
                body: {
                    response: getResponseBody,
                    status: 404,
                },
                status: 404,
            });

            sagaTester = new SagaTester({ initialState, reducers });
            sagaTester.start(AuthSagas.watchPostLogin);
            sagaTester.dispatch(AuthActions.postLoginRequest(authCredentials));

            await sagaTester.waitFor(AuthActionTypes.POST_LOGIN_FAILURE);

            expect(
                sagaTester.wasCalled(AuthActionTypes.POST_LOGIN_FAILURE)
            ).to.equal(true);
            expect(fetchMock.called()).to.be.true;
        }: any)
    );
});
