// @flow

// EMPLOYEES - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */

import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { createMockStore } from 'redux-test-utils';

import {
    EmployeesComponent,
    ConnectedEmployeesComponent,
} from '../../../../src/containers/employee/employee';
import type { Props } from '../../../../src/containers/employee/employee';

const baseProps: Props = {
    employees: [],
    employeeName: '',
    employeeAge: '',
    employeePosition: '',
    getEmployees: () => {},
    filterEl: () => ({}),
};

describe('COMPONENT: <EmployeesComponent />', () => {
    it('should render without errors', () => {
        const wrapper = shallow(<EmployeesComponent {...baseProps} />);

        expect(wrapper).to.exist;
    });
});

// Connected component tests

const mockState = {
    employee: {
        employeeList: [],
        filterCriteria: {
            employeeName: '',
            employeeAge: '',
            employeePosition: '',
        },
    },
};

const mockStore = createMockStore(mockState);

const shallowWithStore = (component, store) => {
    const context = { store };

    return shallow(component, { context });
};

describe('COMPONENT: <ConnectedEmployeesComponent />', () => {
    it('should dispatch an action after the component was mounted', () => {
        const wrapper = shallowWithStore(
            <ConnectedEmployeesComponent />,
            mockStore
        );

        expect(wrapper).to.be.exist;

        const component = wrapper.dive();

        component.instance().componentDidMount();

        expect(
            mockStore.isActionDispatched({
                type: '[EMP] GET_EMPLOYEE_LIST_REQUEST',
            })
        ).to.be.true;
    });
});
