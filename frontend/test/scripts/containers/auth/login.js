// @flow
// LOGIN - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */

import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { toast } from 'react-toastify';

import { LoginComponent } from '../../../../src/containers/auth/login';
import type { Props } from '../../../../src/containers/auth/login';

const baseProps: Props = {
    history: {
        push: () => {},
    },
    errorAuth: 'error',
    userLoggedIn: false,
    initialLoad: false,
    fetching: false,
};

describe('COMPONENT: <LoginComponent />', () => {
    it('should render without errors', () => {
        const wrapper = shallow(<LoginComponent {...baseProps} />);

        expect(wrapper).to.exist;
    });

    it('should call inner redirect method if user is successfully logged in on first component render', () => {
        const loggedInProps = {
            ...baseProps,
            errorAuth: '',
            userLoggedIn: true,
        };

        const wrapper = shallow(<LoginComponent {...loggedInProps} />);

        const redirectTo = sinon.spy(wrapper.instance(), 'redirectTo');

        wrapper.instance().componentDidMount();

        expect(wrapper.instance().props.userLoggedIn).to.be.true;
        expect(redirectTo).to.be.calledWith('/accounts');

        redirectTo.restore();
    });

    it('should NOT call inner redirect method if user is NOT successfully logged in on first component render', () => {
        const notLoggedInProps = {
            ...baseProps,
            errorAuth: '',
            userLoggedIn: false,
        };

        const wrapper = shallow(<LoginComponent {...notLoggedInProps} />);

        const redirectTo = sinon.spy(wrapper.instance(), 'redirectTo');
        const cdm = sinon
            .stub(wrapper.instance(), 'componentDidMount')
            .returns(false);

        // wrapper.instance().componentDidMount();
        cdm();

        expect(cdm).to.have.been.calledOnce;
        expect(redirectTo.callCount).to.be.equal(0);

        cdm.restore();
        redirectTo.restore();
    });

    it('should call inner redirect method if user is successfully logged in', () => {
        const props = { ...baseProps, errorAuth: '' };
        const wrapper = shallow(<LoginComponent {...props} />);

        const redirectTo = sinon.spy(wrapper.instance(), 'redirectTo');

        const newProps = {
            ...props,
            userLoggedIn: true,
        };

        wrapper.setProps(newProps);

        expect(wrapper.instance().props.userLoggedIn).to.be.true;
        expect(redirectTo).to.be.calledWith('/accounts');

        redirectTo.restore();
    });

    it('should redirect to /accounts within history.push() method', () => {
        const props = { ...baseProps, errorAuth: '' };
        const wrapper = shallow(<LoginComponent {...props} />);

        const historyPush = sinon.spy(wrapper.instance().props.history, 'push');

        const newProps = {
            ...props,
            userLoggedIn: true,
        };

        wrapper.setProps(newProps);
        expect(historyPush).to.be.calledWith('/accounts');
        historyPush.restore();
    });

    it('should display error after attempt to login with wrong credentials', () => {
        const props = { ...baseProps, errorAuth: '' };

        const wrapper = shallow(<LoginComponent {...props} />);

        const toastError = sinon.spy(toast, 'error');

        const newProps = { ...props, errorAuth: 'Test Toast Error' };

        wrapper.instance().componentWillReceiveProps(newProps);

        expect(toastError).to.be.calledWith('Test Toast Error', {
            position: toast.POSITION.TOP_RIGHT,
            hideProgressBar: true,
            closeButton: false,
        });

        toastError.restore();
    });
});
