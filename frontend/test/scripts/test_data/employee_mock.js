export default [
  {
    name: 'Jason',
    position: 'dev',
    age: 25,
  },
  {
    name: 'Mark',
    position: 'junior dev',
    age: 20,
  },
  {
    name: 'Lorem',
    position: 'architector',
    age: 45,
  },
  {
    name: 'Roy',
    position: 'developer',
    age: 29,
  },
];
