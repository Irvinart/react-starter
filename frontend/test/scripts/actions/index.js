// @flow
// REDUX ACTIONS - TEST
// ====================
// Make sure all redux action types are globally unique.

const fs = require('fs');
const path = require('path');

function loadActionsFromDir(dirPath: string): Map<string, *> {
    const fullDirPath = path.join(__dirname, dirPath);
    const directoryActions: Map<string, *> = new Map();

    fs.readdirSync(fullDirPath).filter(file =>
        file.endsWith('js')).forEach((file) => {
        // flow-disable-next-line Need to import all the files dynamically.
        directoryActions.set( file, require(`${fullDirPath}/${file}`) ); // eslint-disable-line
    });
    return directoryActions;
}

function compareActionTypesFiles(...typeDirectories: Array<Map<string, Object>>): number {
    const combinedTypes: Map<string, string> = new Map();
    let duplicateActionTypes: number = 0;
    typeDirectories.forEach((typeDirectoryFiles: Map<string, Object>) => {
        typeDirectoryFiles.forEach((actionTypeFile: Object, actionTypeFileName: string) => {
            Object.keys(actionTypeFile).forEach((actionKey: string) => {
                const actionString: string = actionTypeFile[actionKey];

                if (typeof actionString === 'string' && actionString) {
                    if (combinedTypes.has(actionString)) {
                        const previousUse: ?string = combinedTypes.get(actionString);
                        console.warn(`Duplicate action type ${actionString} found in ${actionTypeFileName}. Previously used in ${previousUse || 'unknown file'}.`);
                        duplicateActionTypes += 1;
                    } else {
                        combinedTypes.set(actionString, actionTypeFileName);
                    }
                }
            });
        });
    });
    return duplicateActionTypes;
}

const allHandwrittenActionTypeFiles: Map<string, *> = loadActionsFromDir('../../../src/actions/types');

const duplicateActionTypes: number = compareActionTypesFiles(allHandwrittenActionTypeFiles);
if (!duplicateActionTypes) console.log('No duplicate action types found.');

process.exit(duplicateActionTypes);
