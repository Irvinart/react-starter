// @flow
// LOGIN FORM - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */

import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { createMockStore } from 'redux-test-utils';
import { TextField } from 'material-ui';

import {
    LoginFormComponent,
    ConnectedLoginForm,
} from '../../../../src/components/forms/LoginForm/LoginForm';
import type { Props } from '../../../../src/components/forms/LoginForm/LoginForm';

const baseProps: Props = {
    fetching: true,
    login: () => {},
};

describe('COMPONENT: <LoginFormComponent />', () => {
    it('should render without errors', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        expect(wrapper).to.exist;
    });

    it('should change state when username is changed', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        wrapper.instance().onChangeForm('username', 'test@test.com');

        expect(wrapper.state().username).to.equal('test@test.com');
    });

    it('should change state when password is changed', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        wrapper.instance().onChangeForm('password', 'abcabcacb');

        expect(wrapper.state().password).to.equal('abcabcacb');
    });

    it('should call handler function on username input change', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        const spy = sinon.spy(wrapper.instance(), 'onChangeForm');

        wrapper
            .find(TextField)
            .first()
            .simulate('change', 'username', 'test@test.com');

        expect(spy).to.have.been.calledWith('username', 'test@test.com');

        spy.restore();
    });

    it('should call handler function on password input change', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        const spy = sinon.spy(wrapper.instance(), 'onChangeForm');

        wrapper
            .find(TextField)
            .last()
            .simulate('change', 'password', 'qwerty');

        expect(spy).to.have.been.calledWith('password', 'qwerty');

        spy.restore();
    });

    it('should call validate function after clicking the submit button', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        const isFormValidate = sinon.spy(wrapper.instance(), 'isFormValidate');

        wrapper.find('form').simulate('submit', { preventDefault: () => {} });

        expect(isFormValidate).to.have.been.calledOnce;

        isFormValidate.restore();
    });

    it('should show validation errors is form field are empty', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        wrapper.find('form').simulate('submit', { preventDefault: () => {} });

        expect(wrapper.state().username_error).to.be.equal(
            'This field is required!'
        );
        expect(wrapper.state().password_error).to.be.equal(
            'This field is required!'
        );

        wrapper
            .find(TextField)
            .first()
            .simulate('change', 'username', 'test@test.com');
        wrapper
            .find(TextField)
            .last()
            .simulate('change', 'password', '');

        wrapper.find('form').simulate('submit', { preventDefault: () => {} });

        expect(wrapper.state().username_error).to.be.equal('');
        expect(wrapper.state().password_error).to.be.equal(
            'This field is required!'
        );

        wrapper
            .find(TextField)
            .first()
            .simulate('change', 'username', '');
        wrapper
            .find(TextField)
            .last()
            .simulate('change', 'password', 'qwerty');

        wrapper.find('form').simulate('submit', { preventDefault: () => {} });

        expect(wrapper.state().username_error).to.be.equal(
            'This field is required!'
        );
        expect(wrapper.state().password_error).to.be.equal('');
    });

    it('should call submit function after changing credentials and clicking the submit button', () => {
        const wrapper = shallow(<LoginFormComponent {...baseProps} />);

        const handleOnSubmitForm = sinon.spy(
            wrapper.instance(),
            'handleOnSubmitForm'
        );

        wrapper
            .find(TextField)
            .first()
            .simulate('change', 'username', 'test@test.com');
        wrapper
            .find(TextField)
            .last()
            .simulate('change', 'password', 'qwerty');

        wrapper.find('form').simulate('submit', { preventDefault: () => {} });

        expect(handleOnSubmitForm).to.have.been.calledOnce;

        handleOnSubmitForm.restore();
    });
});

const mockState = {
    user: { fetching: true },
};

const mockStore = createMockStore(mockState);

const shallowWithStore = (component, store) => {
    const context = { store };

    return shallow(component, { context });
};

describe('COMPONENT: <ConnectedLoginForm />', () => {
    it('should dispatch login action within successfull form submit', () => {
        const wrapper = shallowWithStore(<ConnectedLoginForm />, mockStore);

        const component = wrapper
            .dive()
            .setState({ username: 'test@test.com', password: 'qwerty' });

        component.find('form').simulate('submit', { preventDefault: () => {} });

        expect(
            mockStore.isActionDispatched({
                type: '[AUTH] POST_LOGIN_REQUEST',
                payload: {
                    username: 'test@test.com',
                    password: 'qwerty',
                },
            })
        ).to.be.true;
    });
});
