// @flow
// FOOTER - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */

import { expect } from 'chai';
import { shallow } from 'enzyme';
import React from 'react';
import Footer from '../../../../src/components/common/Footer/Footer';

describe('COMPONENT: <Footer />', () => {
    it('should render without errors', () => {
        const wrapper = shallow(<Footer />);

        expect(wrapper).to.exist;
    });
});
