// @flow
// HEADER - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */

import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Header from '../../../../src/components/common/Header/Header';

const baseProps = {
    userLoggedIn: true,
    toggleGoogleBing: true,
    logout: () => {},
    changeToggle: () => {},
};

describe('COMPONENT: <Header />', () => {
    it('should render without errors', () => {
        const wrapper = shallow(<Header {...baseProps} />);

        expect(wrapper).to.exist;
    });
});
