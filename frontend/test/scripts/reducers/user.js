// @flow
// USER (REDUCER) - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */

import { expect } from 'chai';

// COMPONENT
import user, { initialState } from '../../../src/reducers/user';
import * as AuthActions from '../../../src/actions/auth';

import AuthSuccess from '../test_data/auth_success.json';

describe('REDUCER: user', () => {
    it('should return the initial state', () => {
        expect(user(undefined, { type: '' })).to.equal(initialState);
    });

    it('should handle POST_LOGIN_REQUEST', () => {
        const state = {
            ...initialState,
        };
        const userState = user(state, AuthActions.postLoginRequest());

        expect(userState.accessToken).to.be.empty;
        expect(userState.error).to.be.empty;
        expect(userState.fetching).to.be.true;
    });

    it('should handle POST_LOGIN_SUCCESS', () => {
        const state = {
            ...initialState,
            accessToken: '',
            error: 'error',
            fetching: true,
            loggedIn: false,
        };
        const payload = AuthSuccess;
        const userState = user(state, AuthActions.postLoginSuccess(payload));

        expect(userState.accessToken).to.be.equal(payload.token);
        expect(userState.error).to.be.empty;
        expect(userState.fetching).to.be.false;
        expect(userState.loggedIn).to.be.true;
    });

    it('should handle POST_LOGIN_FAILURE', () => {
        const state = {
            ...initialState,
            accessToken: 'token',
            error: '',
            fetching: true,
        };
        const payload = {
            non_field_errors: ['error1', 'error2'],
        };
        const userState = user(state, AuthActions.postLoginFailure(payload));

        expect(userState.error).to.be.equal(
            payload.non_field_errors.join('\n')
        );
        expect(userState.accessToken).to.be.empty;
        expect(userState.fetching).to.be.false;
    });

    it('should handle USER_LOGOUT_REQUEST', () => {
        const state = {
            accessToken: 'token',
            loggedIn: true,
            fetching: true,
            error: 'error',
        };
        const userState = user(state, AuthActions.userLogoutRequest());

        expect(userState.accessToken).to.be.equal(initialState.accessToken);
        expect(userState.loggedIn).to.be.equal(initialState.loggedIn);
        expect(userState.fetching).to.be.equal(initialState.fetching);
        expect(userState.error).to.be.equal(initialState.error);
    });
});
