// @flow
// ENVIRONMENT (REDUCER) - TEST
// =============================================================================
/* eslint no-unused-expressions: [0, { "allowShortCircuit": false, "allowTernary": false }] */

import { expect } from 'chai';

// COMPONENT
import environment, { initialState } from '../../../src/reducers/environment';

describe('REDUCER: environment', () => {
    it('should return the initial state', () => {
        expect(environment(undefined, { type: '' })).to.equal(initialState);
    });
});
