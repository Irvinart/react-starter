// DEVELOPMENT BABEL SHIM
// =============================================================================
// Use babel-register to JIT transpile the ES6/ES7 code in the webapp. It's
// essentially a convenience shim, otherwise all webapp code would have to be
// written Node compatible (annoying), not rendered serverside (bad for SEO), or
// transpiled and then served (slow for development).

const path = require('path');

let config;

try {
    // Worth noting that the .babelrc specified in this file is slightly different
    // from what the webpack config specifies. This is due to the webpack-hmr
    // transform needing to be present in the babel loader, but not in the server.

    config = JSON.parse(require('fs').readFileSync('.babelrc')); // eslint-disable-line  global-require
} catch (error) {
    console.error('Error parsing .babelrc: ', error);
}

require('babel-register')(config);
require('babel-polyfill');
require('./virtualDOM');

process.argv.forEach((val) => {
    if (val.startsWith('--entry=')) {
        require(path.resolve(val.split('=').pop())); // eslint-disable-line
    }
});
