/* eslint-disable */
declare class describe {
    static (description: string, spec: () => void): void;
}

declare class it {
    static (description: string, spec: () => void): void;
}

declare class before {
    static ( func: () => void ) : void;
}

declare class after {
    static ( func: () => void ) : void;
}

declare class beforeEach {
    static ( func: () => void ) : void;
}

declare class afterEach {
    static ( func: () => void ) : void;
}
