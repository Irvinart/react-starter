// =============================================================================
// This is needed in order to create a virtual DOM for testing. Many React
// components require a mounted DOM instance in order to properly test their
// lifecycle behavior, and cannot rely purely on a shallow test.

const jsdom = require('jsdom');

const exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom.jsdom('');
global.window = document.parentWindow;

Object.keys(document.defaultView).forEach((property) => {
    if (typeof global[property] === 'undefined') {
        exposedProperties.push(property);
        global[property] = document.defaultView[property];
    }
});

Object.defineProperties(window.HTMLElement.prototype, {
    offsetLeft: {
        get() { return parseFloat(window.getComputedStyle(this).marginLeft) || 0; },
    },
    offsetTop: {
        get() { return parseFloat(window.getComputedStyle(this).marginTop) || 0; },
    },
    offsetHeight: {
        get() { return parseFloat(window.getComputedStyle(this).height) || 0; },
    },
    offsetWidth: {
        get() { return parseFloat(window.getComputedStyle(this).width) || 0; },
    },
});

global.navigator = {
    userAgent: 'node.js',
};
