// MOCHA TEST ENTRY POINT
// =============================================================================
// Programmatically executes all Mocha tests after performing global setup for
// things like schema loading, chai plugins, and reporter selection.

require('babel-register');
require('isomorphic-fetch');


const { error } = console;
console.error = function throwErrorOnPropTypeWarning(warning, ...args) {
    if (/(Invalid prop|Failed prop type)/.test(warning)) {
        throw new Error(warning);
    }
    error.apply(console, [warning, ...args]);
};

const Mocha = require('mocha');
const fs = require('fs');
const path = require('path');
const chai = require('chai');
const enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16/build/ReactSixteenAdapter');

// CONFIGURE ENZYME
enzyme.configure({ adapter: new Adapter() });

// STATIC ASSETS
// These have to be no-oped during testing, since webpack isn't being used
const noOp = () => 1;

require.extensions['.woff'] = noOp;
require.extensions['.woff2'] = noOp;
require.extensions['.css'] = noOp;
require.extensions['.scss'] = noOp;
require.extensions['.png'] = noOp;
require.extensions['.jpg'] = noOp;
require.extensions['.jpeg'] = noOp;
require.extensions['.gif'] = noOp;
require.extensions['.svg'] = noOp;


// MOCHA OPTIONS
const mochaOptions = {};

process.argv.forEach((val) => {
    if (val.startsWith('--reporter=')) {
        mochaOptions.reporter = require(path.resolve(val.split('=').pop())); // eslint-disable-line
    }
});


// CONFIGURE MOCHA
const mocha = new Mocha(mochaOptions);

function loadTestDir(dirPath) {
    const workingPath = path.join(__dirname, dirPath);

    fs.readdirSync(workingPath).filter(file =>
        file.endsWith('.js')).forEach((file) => {
        mocha.addFile(path.join(workingPath, file));
    });
}


// LOAD CHAI PLUGINS
chai.use(require('chai-enzyme'));
chai.use(require('chai-json-schema'));
chai.use(require('sinon-chai'));

chai.should();

chai.tv4.banUnknown = true;
chai.tv4.multiple = true;


// LOAD TESTS (in alphabetical order)
// Components
loadTestDir('./scripts/components/common');
loadTestDir('./scripts/components/login');

// Containers
loadTestDir('./scripts/containers/auth');
loadTestDir('./scripts/containers/employee');

// Reducers
loadTestDir('./scripts/reducers');

// Sagas
loadTestDir('./scripts/sagas');

// RUN TESTS
mocha.run((failures) => {
    process.on('exit', () => {
        process.exit(failures);
    });
});
